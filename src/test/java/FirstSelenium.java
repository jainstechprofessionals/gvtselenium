import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FirstSelenium {

	public static void main(String[] args) {

		// All the software should installed into the system
		// Find the browser
		// Enter the URL
		// Enter the user name
		// Enter the password
		// Verify the login

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GvtSelenium1\\src\\main\\resources\\ChromeDriver\\chromedriver.exe");

		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://demo.testfire.net/login.jsp");
		
		
		driver.findElement(By.xpath("//input[@id='uid']")).sendKeys("jsmith");
		driver.findElement(By.id("passw")).sendKeys("demo1234");
		driver.findElement(By.name("btnSubmit")).click();
		
		String str =driver.findElement(By.xpath("//h1[contains(text(),'Hello')]")).getText();
		System.out.println(str);
		
		if(str.contains("John")){
			System.out.println("Test case passed");
		}else {
			System.out.println("Test case failed");
		}
		
		
		// Advance  xpath
		
	//1. Contains
	//2. starts-with  : //h1[starts-with(text(),'Hello')]
		
		
	}

}
